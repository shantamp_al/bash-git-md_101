# Bash, git, bitbucket and Markdown Intro 
This is a lesson to introduce the follown technologies that we'll be using:

- git
- bitbucket
- bash
- markdown (.md)

------------------------------------------------

### Bash

Bash is a Unix shell and command language.

<u>Main Commands</u>

**# Change directory** 

` $ cd <folder> `

**# Check current directory** 

` $ pwd `

**# Create new directory** 

` $ mkdir <name> `

**# Remove directory** 

` $ rm -rf <directory> `

**# list all folders and files within directory** 

` $ ls -a `

**# Create file within directory** 

` $ touch <name> `

**# Read content within file**

` $ cat <filename> `


### Git

Git is software for tracking changes in any set of files, usually used for coordinating work among programmers collaboratively developing source code during software development. Its goals include speed, data integrity, and support for distributed, non-linear workflows.

<u>Main Commands</u>

**# To start using git** 

` $ git init `

**# To see if there is any outstanding changes**

` $ git status `

**# To add changes to timeline**

` $ git add <filename> /OR . `

**# To apply changes**

` $ git commit -m " whatchangesmade" `

**# To connect your repo with bitbucket**

` $ git remote add origin git@bitbucket.org:shantamp_al/<reponame>.git `

•• To check if your remote repo is connected

` $ git remote --v `

**# To push updates to your Master branch** 

` $ git push origin master `

### Additional Information.

If you have a folder with files that you do not wish to add to your online repository, you can add a ` .gitignore ` which you can write the file names you would like `git` to ignore when commiting changes.


### Bitbucket 
Bitbucket is a Git-based source code repository hosting service. This is where you can save your code online and can be used to collaborate with others.

To use bitbucket you will need to...

1. Create an account.
2. create a repository (private or public)
3. generate a ssh key on your CLI and add to security in bitbucket.
4. Once public key added, use ` git remote add ... `

You are now ready to push your code online.

### Markdown

###### Heading 
##### Heading
#### Heading
### Heading
## Heading

**BOLD**

*ITALIC*

<u>Underline</u>

> Block Quote

` code `

<u>Ordered list</u>

1. First Item
2. Second Item
3. Third Item

<u> Unordered list</u>
 - First Item
 - Second Item
 - Third Item













